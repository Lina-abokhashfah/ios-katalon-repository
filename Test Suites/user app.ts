<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>user app</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>e5a3f97b-7268-4cdd-a087-e35af8014727</testSuiteGuid>
   <testCaseLink>
      <guid>1a75804d-04f9-4c0a-b9d5-99b114b98413</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Intrest</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c8c877b-129a-4947-adcf-740438267eb4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Complete profile -personal info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abd923b8-95f5-438f-94bd-200d3b16798a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/complete profile - Drug</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e78361be-32f2-4bec-8da0-b2ccedf88559</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Onboarding pages</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5e2e7522-ff9a-422c-8801-8f5616d77527</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Ask free question</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9e57484a-08bf-4212-ada7-4732f5942abd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/complete profile - History</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6be8a13a-f709-4f0b-9aa3-251b7aaedc2f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/complete profile - surgery</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2b222782-2ddc-4ef4-bf82-64912e4f5828</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>c0389aa1-a0e3-4dc1-be83-51419f6c710d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/CTA Consultaion view , open Dr profile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e76c04b6-371a-44ea-998d-9bda51533ca2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/show subscription info</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e327fc36-991e-448c-9a97-d776fdb14f02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change language from setting</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df544a74-1e9d-42e1-ae01-f164e83f6407</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/complete profile - pregnancy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b2a74f2c-f8ca-4669-966f-fb26eebfd616</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/complete profile - Chronic diseases</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13a6d93f-aa10-4e4b-ac75-92f6049545a4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Onboarding change languge</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f0998344-532f-4fb6-b4a5-f65178751c32</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9abc5e5c-d806-4f84-9b4e-1ed3cefe16e7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Add new phr</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2dbfa216-bd84-47f5-b5d3-094cb54bdeab</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
