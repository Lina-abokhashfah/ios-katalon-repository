import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

Thread.sleep(2000)

Mobile.tap(findTestObject('Complete Profile - personal info/profile'), 0)

Thread.sleep(2000)

Mobile.tap(findTestObject('add new phr/Add new phr icon'), 0)

Mobile.tap(findTestObject('Subscription information/Subscription info button'), 0)

Mobile.verifyElementExist(findTestObject('Subscription information/Current subscription text verification'), 0)

Mobile.verifyElementExist(findTestObject('Subscription information/Subscription log text verification'), 0)

Mobile.tap(findTestObject('Subscription information/Cancel subscription button'), 0)

Mobile.verifyElementExist(findTestObject('Subscription information/Cancel subscription text verifivcation'), 0)

Mobile.closeApplication()

