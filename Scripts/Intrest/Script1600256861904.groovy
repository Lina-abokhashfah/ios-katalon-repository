import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Complete Profile - personal info/profile'), 0)

Mobile.tap(findTestObject('add new phr/Add new phr icon'), 0)

Mobile.tap(findTestObject('interset/Intrest button'), 0)

Mobile.tap(findTestObject('interset/Add intrest'), 0)

Mobile.setText(findTestObject('interset/Intrest text field'), 'جلد', 0)

Mobile.hideKeyboard()

Thread.sleep(2000)

Mobile.tap(findTestObject('interset/Interest_Skin'), 0)

Thread.sleep(2000)

Mobile.tap(findTestObject('interset/Save intrest button'), 0)

Mobile.tap(findTestObject('interset/Edit intrest btton'), 0)

Thread.sleep(2000)

Mobile.tap(findTestObject('interset/Interest_Skin'), 0)

Mobile.tap(findTestObject('interset/Save button after remove intrest'), 0)

Mobile.closeApplication()

