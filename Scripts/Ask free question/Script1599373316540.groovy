import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Ask free question/profile'), 0)

Mobile.tap(findTestObject('Ask free question/My free question tab'), 0)

Thread.sleep(2000)

Mobile.tap(findTestObject('Ask free question/Ask button'), 0)

Mobile.setText(findTestObject('Ask free question/Free question title'), 'اعاني من الم شديد في الظهر وارتفاع. في درجه الحراره ي', 
    0)

Mobile.setText(findTestObject('Ask free question/Free question text'), 'ارتفاع بدرجه الحراره وسخونه والم شديده في الراس وتست هاد السؤال قصص', 
    0)

Mobile.tap(findTestObject('Ask free question/Free question text to hide keyboard'), 0)

Thread.sleep(3000)

Mobile.tap(findTestObject('Ask free question/send free'), 0)

Mobile.tap(findTestObject('Ask free question/Question releated to me button'), 0)

Mobile.tap(findTestObject('Ask free question/No allergy button'), 0)

Mobile.tap(findTestObject('Ask free question/No chronic diseases button'), 0)

Mobile.tap(findTestObject('Ask free question/No drug button'), 0)

Mobile.verifyElementExist(findTestObject('Ask free question/Send free question button'), 0)

Mobile.closeApplication()

