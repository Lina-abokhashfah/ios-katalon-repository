import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

Thread.sleep(2000)

Mobile.tap(findTestObject('Complete Profile - personal info/profile'), 0)

Thread.sleep(4000)

Mobile.tap(findTestObject('tipc_test/add_phr_btn'), 0)

Mobile.setText(findTestObject('add new phr/Phr name text field'), 'new', 0)

Mobile.tap(findTestObject('add new phr/Birthdate'), 0)

Mobile.tap(findTestObject('add new phr/Select Birthdate'), 0)

Mobile.tap(findTestObject('test/Relation'), 0)

Mobile.tap(findTestObject('test/Select releation'), 0)

Mobile.tap(findTestObject('add new phr/Save add new phr'), 0)

Thread.sleep(2000)

Mobile.verifyElementVisible(findTestObject('add new phr/Veriy add phr successfuly pop-up'), 0)

Mobile.tap(findTestObject('add new phr/Veriy add phr successfuly pop-up'), 0)

Mobile.closeApplication()

