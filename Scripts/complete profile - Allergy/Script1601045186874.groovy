import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

Thread.sleep(2000)

Mobile.tap(findTestObject('Complete Profile - personal info/profile'), 0)

Mobile.tap(findTestObject('Complete profile - Chronic diseases/Complete profile button'), 0)

Mobile.tap(findTestObject('Complete progile - Allergy/Allergy button'), 0)

Mobile.tap(findTestObject('Complete progile - Allergy/Add allergy'), 0)

Mobile.setText(findTestObject('Complete progile - Allergy/Allergy text field'), 'حساسي', 0)

Mobile.tap(findTestObject('Complete progile - Allergy/Select Allergy'), 0)

Mobile.setText(findTestObject('Complete progile - Allergy/Comment Of Allergy'), 'ملاحظات عامه', 0)

Mobile.tap(findTestObject('Complete progile - Allergy/Text to hide keyboard allergy'), 0)

Thread.sleep(2000)

Mobile.tap(findTestObject('Complete progile - Allergy/Save allergy'), 0)

Mobile.tap(findTestObject('Complete progile - Allergy/Ok for save allergy'), 0)

Mobile.tap(findTestObject('Complete progile - Allergy/Remove allergy'), 0)

Mobile.tap(findTestObject('Complete progile - Allergy/Yes for remove allergy'), 0)

Mobile.closeApplication()

