import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('Login'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('Ask free question/profile'), 0)

Mobile.tap(findTestObject('Complete profile - Chronic diseases/Complete profile button'), 0)

Mobile.tap(findTestObject('Complete profile - Chronic diseases/Chronic disease'), 0)

Mobile.tap(findTestObject('Complete profile - Chronic diseases/Add chronic diseases'), 0)

Mobile.setText(findTestObject('Complete profile - Chronic diseases/Chronic diseases text'), 'مرض', 0)

Mobile.tap(findTestObject('Complete profile - Chronic diseases/Select chronic diseases'), 0)

Mobile.tap(findTestObject('Complete profile - Chronic diseases/Text to hide the keyboard'), 0)

Mobile.tap(findTestObject('Complete profile - Chronic diseases/Save Chronic diseases'), 0)

Mobile.tap(findTestObject('Complete profile - Chronic diseases/Ok button after save chronic diseases'), 0)

Mobile.tap(findTestObject('Complete profile - Chronic diseases/Remove chronic diseases'), 0)

Mobile.tap(findTestObject('Complete profile - Chronic diseases/Yes for delete diseases'), 0)

Mobile.closeApplication()

